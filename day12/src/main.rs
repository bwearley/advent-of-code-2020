use std::env;
use std::io::{prelude::*, BufReader};
use std::fs::File;

#[derive(Debug)]
struct Action {
    direction: Direction,
    magnitude: i64,
}
impl From<&String> for Action {
    fn from(s: &String) -> Self {
        use Direction::*;
        Self {
            direction: match s.chars().next().unwrap() {
                'N' => North,
                'S' => South,
                'E' => East,
                'W' => West,
                'L' => Left,
                'R' => Right,
                'F' => Forward,
                other => panic!("Unknown character: {}", other),
            },
            magnitude: s[1..].parse().unwrap(),
        }
    }
}
#[derive(Debug)]
enum Direction {
    North,
    South,
    East,
    West,
    Left,
    Right,
    Forward,
}

// Rotation matrices
const ROTATE_CW: (i64,i64,i64,i64) = (0, 1, -1, 0);
const ROTATE_CCW: (i64,i64,i64,i64) = (0, -1, 1, 0);

fn rotation_matmul(m2x2: (i64,i64,i64,i64), m2x1: (i64,i64)) -> (i64,i64) {
    (m2x2.0 * m2x1.0 + m2x2.1 * m2x1.1, m2x2.2 * m2x1.0 + m2x2.3 * m2x1.1)
}

fn day12(input: &str) {
    use Direction::*;
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);
    let input: Vec<_> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let directions = input
        .iter()
        .map(Action::from)
        .collect::<Vec<_>>();

    // Part 1
    let mut facing = (1,0);
    let mut pos = (0,0);
    for dir in &directions {
        match dir.direction {
            North => { pos.1 += dir.magnitude }, 
            South => { pos.1 -= dir.magnitude },
            East  => { pos.0 += dir.magnitude },
            West  => { pos.0 -= dir.magnitude },
            Left  => {
                for _ in 0..dir.magnitude / 90 {
                    facing = rotation_matmul(ROTATE_CCW,facing);
                }
            },
            Right => {
                for _ in 0..dir.magnitude / 90 {
                    facing = rotation_matmul(ROTATE_CW,facing);
                }
            },
            Forward => {
                pos.0 += dir.magnitude * facing.0;
                pos.1 += dir.magnitude * facing.1;
            },
        }
    }
    let part1 = (pos.0).abs() + (pos.1).abs();
    println!("Part 1: {}", part1); // 962

    // Part 2
    let mut pos = (0,0);
    let mut waypoint = (10,1);
    for dir in &directions {
        match dir.direction {
            North => { waypoint.1 += dir.magnitude }, 
            South => { waypoint.1 -= dir.magnitude },
            East  => { waypoint.0 += dir.magnitude },
            West  => { waypoint.0 -= dir.magnitude },
            Left  => {
                for _ in 0..dir.magnitude / 90 {
                    waypoint = rotation_matmul(ROTATE_CCW,waypoint);
                }
            },
            Right   => {
                for _ in 0..dir.magnitude / 90 {
                    waypoint = rotation_matmul(ROTATE_CW,waypoint);
                }
            },
            Forward => {
                pos.0 += dir.magnitude * waypoint.0;
                pos.1 += dir.magnitude * waypoint.1;
            },
        }
    }
    let part2 = (pos.0).abs() + (pos.1).abs();
    println!("Part 2: {}", part2); // 56135
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day12(&filename);
}