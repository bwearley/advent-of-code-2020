use std::env;
use std::io;
use std::collections::HashMap;

extern crate itertools;
use itertools::Itertools;

fn day06(input: &str) -> io::Result<()> {
    
    // Part 1
    let part1: usize = std::fs::read_to_string(input)
        .unwrap()
        .split("\n\n")
        .map(|group| group.replace("\n","").chars().unique().count())
        .sum();

    println!("Part 1: {}", part1); // 6532

    // Part 2
    let mut ans: Vec<HashMap<char,usize>> = Vec::new();
    let input_str = std::fs::read_to_string(input).unwrap();
    let answers: Vec<_> = input_str.split("\n\n").collect();

    let mut part2 = 0;
    for (i,group) in answers.iter().enumerate() {
        ans.push(HashMap::new());
        let group_size = &group.split("\n").collect::<Vec<_>>().len();
        for person in group.split("\n") {
            for a in person.chars() {
                *ans[i].entry(a).or_insert(0) += 1;
            }
        }
        part2 += ans[i].iter().filter(|(_,v)| v == &group_size).count();
    }

    println!("Part 2: {}", part2); // 3427

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day06(&filename).unwrap();
}