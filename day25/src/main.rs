use std::env;
use std::io::{prelude::*, BufReader};
use std::fs::File;

const DIVISOR: usize = 20201227;
const SUBJECT: usize = 7;

fn transform(subject: usize, loop_size: usize) -> usize {
    let mut value = 1;
    for _ in 0..loop_size {
        value *= subject;
        value %= DIVISOR;
    }
    value
}

fn day25(input: &str) {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let keys = input.iter().map(|x| x.parse::<usize>().unwrap()).collect::<Vec<_>>();

    let mut loops = vec![0, 0];
    let mut value = 1;
    for loop_size in 1.. {
        value *= SUBJECT;
        value %= DIVISOR;
        if value == keys[0] { loops[0] = loop_size; }
        if value == keys[1] { loops[1] = loop_size; }
        if loops[0] != 0 && loops[1] != 0 { break; }
    }
 
    println!("Part 1: {}", transform(keys[0],loops[1])); // 9420461
    println!("Verification: {}", transform(keys[1],loops[0]));

}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day25(&filename);
}