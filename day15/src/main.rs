use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::HashMap;

fn day15(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input: Vec<_> = input
        .concat()
        .split(",")
        .map(str::parse::<usize>)
        .map(Result::unwrap)
        .collect();

    let mut announced: HashMap<usize,usize> = HashMap::new();
    let mut turn = 0;
    let mut last;
    let mut this = 0;

    // Preload inputs
    for (i,num) in input.iter().enumerate() {
        turn = i + 1;
        if turn != input.len() { announced.insert(*num,turn); }
    }
    last = *input.iter().last().unwrap();

    // Play game
    loop {
        if turn == 2020     { println!("Part 1: {}", this); } // 260
        if turn == 30000000 { println!("Part 2: {}", this); break; } // 950
        this = match announced.get(&last) {
            Some(v) => turn - v,
            None => 0,
        };
        announced.insert(last,turn);
        turn += 1;
        last = this;
    }
    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day15(&filename).unwrap();
}