use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

fn calc_p1(input: &str, start: usize) -> (i64, usize) {
    let mut result: i64 = 0;
    let mut next: i64 = 0;
    let mut ix = start;
    let mut op = '+';
    let chars: Vec<char> = input.chars().collect();
    loop {
        match chars[ix] {
            ' ' => {ix +=1; continue;},
            '+' | '*' => { op = chars[ix]; },
            '(' => { let r = calc_p1(&input,ix+1); next = r.0; ix = r.1; },
            ')' => { return (result,ix); },
            '0'..='9' => {
                next = chars[ix].to_digit(10).unwrap() as i64;
            },
            other => panic!("Parse error 1 at: {}", other),
        }
        match chars[ix] {
            ')' | '0'..='9' => {
                match op {
                    '+' => { result += next; },
                    '*' => { result *= next; },
                    other => panic!("Parse error 2 at: {}", other),
                }4;
            },
            _ => {},
        }
        ix += 1;
        if ix >= chars.len() { break; }
    }
    (result, ix)
}

fn calc_p2(input: &str) -> i64 {
    let input = format!("({})",input
            .replace("(", "((")
            .replace(")", "))")
            .replace(" * ", ") * ("));
    calc_p1(&input,0).0
}

fn day18(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    // Part 1
    let part1: i64 = input.iter().map(|x| calc_p1(x,0).0).sum();
    println!("Part 1: {}", part1); // 7147789965219

    // Part 2
    let part2: i64 = input.iter().map(|x| calc_p2(x)).sum();
    println!("Part 2: {}", part2); // 136824720421264

    // Part 2
    // This part requires a version of eval hacked to change
    // operator precedence in ./src/operator/mod.rs
    let part2 = input.iter().map(|x| eval::eval(x).unwrap().as_i64().unwrap()).sum::<i64>();
    println!("Part 2 (eval): {}", part2); // 136824720421264

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day18(&filename).unwrap();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part1_tests() {
        assert!(calc_p1("1 + 2 * 3 + 4 * 5 + 6",0).0 == 71);
        assert!(calc_p1("1 + (2 * 3) + (4 * (5 + 6))",0).0 == 51);
        assert!(calc_p1("2 * 3 + (4 * 5)",0).0 == 26);
        assert!(calc_p1("5 + (8 * 3 + 9 + 3 * 4 * 3)",0).0 == 437);
        assert!(calc_p1("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))",0).0 == 12240);
        assert!(calc_p1("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2",0).0 == 13632);
    }

    #[test]
    fn part2_tests() {
        assert!(eval::eval("1 + 2 * 3 + 4 * 5 + 6").unwrap().as_i64().unwrap() == 231);
        assert!(eval::eval("1 + (2 * 3) + (4 * (5 + 6))").unwrap().as_i64().unwrap() == 51);
        assert!(eval::eval("2 * 3 + (4 * 5)").unwrap().as_i64().unwrap() == 46);
        assert!(eval::eval("5 + (8 * 3 + 9 + 3 * 4 * 3)").unwrap().as_i64().unwrap() == 1445);
        assert!(eval::eval("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))").unwrap().as_i64().unwrap() == 669060);
        assert!(eval::eval("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2").unwrap().as_i64().unwrap() == 23340);
    }
}