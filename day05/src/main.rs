use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

struct Seat {
    row: i64,
    col: i64,
}
impl From<&String> for Seat {
    fn from(s: &String) -> Self {
        
        // Get row
        let fb = &s[0..7];
        let mut rows: Vec<i64> = (0..=127).collect();
        for c in fb.chars() {
            let l = rows.len();
            match c {
                'F' => for _ in 0..l/2 { rows.pop(); },
                'B' => for _ in 0..l/2 { rows.remove(0); },
                other => panic!("Unknown character: {}", other),
            }
        }
        
        // Get column
        let lr = &s[7..10];
        let mut cols: Vec<i64> = (0..=7).collect();
        for c in lr.chars() {
            let l = cols.len();
            match c {
                'L' => for _ in 0..l/2 { cols.pop(); },
                'R' => for _ in 0..l/2 { cols.remove(0); },
                other => panic!("Unknown character: {}", other),
            }
        }

        Self { row: rows[0], col: cols[0] }
    }
}
impl Seat {
    pub fn seat_id(&self) -> i64 {
        self.row * 8 + self.col
    }
}

fn day05(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    let seats = input
        .iter()
        .map(Seat::from)
        .collect::<Vec<Seat>>();

    // Part 1
    let part1 = seats.iter().map(Seat::seat_id).max().unwrap();
    println!("Part 1: {}", part1); // 826

    // Part 2
    let mut seats_sorted = seats
        .iter()
        .map(Seat::seat_id)
        .collect::<Vec<_>>();
    seats_sorted.sort();

    let part2 = seats_sorted
        .windows(2)
        .filter(|x| x[1] - x[0] == 2)
        .map(|x| x[0]+1)
        .next()
        .unwrap();
    println!("Part 2: {}", part2); // 678

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day05(&filename).unwrap();
}