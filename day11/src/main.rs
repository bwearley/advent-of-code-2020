use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::fmt;

static ALL_DIRS: &'static [(i64,i64)] = &[(-1,-1),(-1,0),(-1,1),(0,-1),(0,1),(1,-1),(1,0),(1,1)];

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
enum Square {
    Floor,
    Chair,
    Person,
}
impl fmt::Display for Square {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Self::Floor  => write!(f, "."),
            Self::Chair  => write!(f, "L"),
            Self::Person => write!(f, "#"),

        }
    }
}

fn count_neighbors_p1(map: &Vec<Vec<Square>>, x: i64, y: i64, xdim: i64, ydim: i64) -> i64 {
    let mut neighbors = 0;
    for dir in ALL_DIRS {
        let (nx,ny) = (x + dir.0, y + dir.1);
        if (0..xdim).contains(&nx) && (0..ydim).contains(&ny) {
            match map[nx as usize][ny as usize] {
                Square::Person => { neighbors += 1; continue; },
                _ => {},
            }
        } else {
            continue;
        }
    }
    neighbors
}

fn count_neighbors_p2(map: &Vec<Vec<Square>>, x: i64, y: i64, xdim: i64, ydim: i64) -> i64 {
    let mut neighbors = 0;
    'outer: for dir in ALL_DIRS {
        for i in 1.. {
            let (nx,ny) = (x + i*dir.0, y + i*dir.1);
            if (0..xdim).contains(&nx) && (0..ydim).contains(&ny) {
                match map[nx as usize][ny as usize] {
                    Square::Person => { neighbors += 1; continue 'outer; },
                    Square::Chair => continue 'outer,
                    _ => {},
                }
            } else {
                continue 'outer;
            }
        }
    }
    neighbors
}

fn seat_map(input: &Vec<String>) -> Vec<Vec<Square>> {
    let ydim: usize = input.len();
    let xdim: usize = input.iter().next().unwrap().len();
    let mut chairs = vec![vec![Square::Floor; ydim]; xdim];
    for y in 0..input.len() as usize {
        let mut line = input[y].chars();
        for x in 0..xdim as usize {
            match line.next().unwrap() {
                'L' => chairs[x][y] = Square::Chair,
                '.' => chairs[x][y] = Square::Floor,
                '#' => unreachable!("Found a person in input file."),
                other => panic!("Unknown map square: {}", other),
            }
        }
    }
    chairs
}

fn part1(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    // Build map
    let ydim: usize = input.len();
    let xdim: usize = input.iter().next().unwrap().len();
    let mut chairs = seat_map(&input);

    'main_lp: loop {
        let current = chairs.clone();
        let mut changes = 0;
        for y in 0..ydim as usize {
            for x in 0..xdim as usize {

                // Count neighbors
                let people = count_neighbors_p1(&current, x as i64, y as i64, xdim as i64, ydim as i64);

                // If a seat is empty (L) and there are no occupied seats
                // adjacent to it, the seat becomes occupied.
                if current[x][y] == Square::Chair && people == 0 {
                    chairs[x][y] = Square::Person;
                    changes += 1;
                }

                // If a seat is occupied (#) and four or more seats adjacent to it are
                // also occupied, the seat becomes empty.
                if current[x][y] == Square::Person && people >= 4 {
                    chairs[x][y] = Square::Chair;
                    changes += 1;
                }
            }
        }
        // Check if no changes
        if changes == 0 { break 'main_lp; }
    }

    let part1 = chairs
        .iter()
        .flat_map(|x| x.iter().filter(|&x| x == &Square::Person))
        .count();

    println!("Part 1: {}", part1); // 2273

    Ok(())
}

fn part2(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    // Build map
    let ydim: usize = input.len();
    let xdim: usize = input.iter().next().unwrap().len();
    let mut chairs = seat_map(&input);

    'main_lp: loop {
        let current = chairs.clone();
        let mut changes = 0;
        for y in 0..ydim as usize {
            for x in 0..xdim as usize {

                // Count neighbors
                let people = count_neighbors_p2(&current, x as i64, y as i64, xdim as i64, ydim as i64 );

                // If a seat is empty (L) and there are no occupied seats
                // adjacent to it, the seat becomes occupied.
                if current[x][y] == Square::Chair && people == 0 {
                    chairs[x][y] = Square::Person;
                    changes += 1;
                }

                // If a seat is occupied (#) and five or more seats adjacent to it are
                // also occupied, the seat becomes empty.
                if current[x][y] == Square::Person && people >= 5 {
                    chairs[x][y] = Square::Chair;
                    changes += 1;
                }
            }
        }
        // Check if no changes
        if changes == 0 { break 'main_lp; }
    }

    let part2 = chairs
        .iter()
        .flat_map(|x| x.iter().filter(|&x| x == &Square::Person))
        .count();

    println!("Part 2: {}", part2); // 2064

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    part1(&filename).unwrap();
    part2(&filename).unwrap();
}