use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::ops::RangeInclusive;

extern crate regex;
use regex::Regex;

#[derive(Clone, Hash)]
struct PasswordReqPair {
    pub range: RangeInclusive<usize>,
    pub letter: char,
    pub password: String,
}
impl From<&String> for PasswordReqPair {
    fn from(s: &String) -> Self {
        // 3-12 x: pxxxxxqkxhdqk
        let re = Regex::new(r"(\d+)-(\d+) (\w): (\w+)").unwrap();
        let matches = re.captures(s).unwrap();
        Self {
            range:    matches[1].parse().unwrap()..=matches[2].parse().unwrap(),
            letter:   matches[3].parse().unwrap(),
            password: matches[4].parse().unwrap(),
        }
    }
}
impl PasswordReqPair {
    pub fn is_valid_part1(&self) -> bool {
        let count = self.password.chars().filter(|x| x == &self.letter).count();
        self.range.contains(&count)
    }
    pub fn is_valid_part2(&self) -> bool {
        let chars = self.password.chars().collect::<Vec<char>>();
        let first = chars[self.range.start()-1] == self.letter;
        let second = chars[self.range.end()-1] == self.letter;
        first ^ second // xor: (first && !second) || (!first && second)
    }
}

fn day02(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    let pass_data = input
        .iter()
        .map(PasswordReqPair::from)
        .collect::<Vec<PasswordReqPair>>();

    // Part 1
    let part1 = pass_data.iter().filter(|x| x.is_valid_part1()).count();
    println!("Part 1: {}", part1); // 469

    // Part 2
    let part2 = pass_data.iter().filter(|x| x.is_valid_part2()).count();
    println!("Part 2: {}", part2); // 267

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day02(&filename).unwrap();
}