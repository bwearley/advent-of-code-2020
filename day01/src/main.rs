use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

extern crate itertools;
use itertools::Itertools;

fn combo_sum(inputs: &Vec<i64>, num: usize, sum: i64) -> Option<i64> {
    inputs
        .iter()
        .combinations(num)
        .find(|seq| seq.iter().copied().sum::<i64>() == sum)
        .map(|x| x.iter().copied().product())
}

fn day01(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input: Vec<i64> = input
        .iter()
        .map(|x| x.parse::<i64>().unwrap())
        .collect();

    // Part 1
    let part1 = combo_sum(&input, 2, 2020).unwrap();
    println!("Part 1: {}", part1); // 538464

    // Part 2
    let part2 = combo_sum(&input, 3, 2020).unwrap();
    println!("Part 2: {}", part2); // 278783190

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day01(&filename).unwrap();
}