use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::HashMap;

#[macro_use]
extern crate text_io;

fn build_mask(mask_str: &str) -> Vec<(usize,usize)> {
    let mut mask: Vec<(usize,usize)> = Vec::new();
    for (i,bit) in mask_str.chars().enumerate() {
        match bit {
            '0' | '1' => { mask.push((i,bit.to_digit(10).unwrap() as usize)); },
            'X' => {},
            other => panic!("Unknown character in bitmask: {}", other),
        }
    }
    mask
}

fn base10_to_binary_vec(base10: usize) -> Vec<usize> {
    format!("{:036b}", base10)
        .chars()
        .map(|c| c.to_digit(10).unwrap() as usize)
        .collect::<Vec<_>>()
}

fn apply_mask(mask_str: &str, addr_str: &str) -> String {
    let mask: Vec<_> = mask_str.chars().collect();
    let addr: Vec<_> = addr_str.chars().collect();
    let mut result = String::new();
    for i in 0..mask_str.len() {
        match &mask[i] {
            '0' => result.push_str(&addr[i].to_string()),
            '1' => result.push_str("1"),
            'X' => result.push_str("X"),
            other => panic!("Unknown character: {}", other),
        }
    }
    result
}

fn floating_addresses(mask_str: &str, start: usize) -> Vec<String> {
    let mask: Vec<_> = mask_str.chars().collect();
    let mut next: String = mask_str[0..std::cmp::min(start,mask.len())].to_string();
    let mut addrs: Vec<String> = Vec::new();
    for i in start..mask.len() {
        match &mask[i] {
            '0' | '1' => { next.push_str(&mask[i].to_string()) },
            'X' => {
                let mut next0 = next.clone(); next0.push_str("0");
                let mut next1 = next.clone(); next1.push_str("1");
                if i == mask.len() - 1 {
                    addrs.push(next0);
                    addrs.push(next1);
                } else {
                    next0.push_str(&mask_str[i+1..]);
                    next1.push_str(&mask_str[i+1..]);
                    for a in floating_addresses(&next0, i+1) {
                        addrs.push(a);
                    }
                    for a in floating_addresses(&next1, i+1){
                        addrs.push(a);
                    }
                }
            },
            other => panic!("Unknown character: {}", other),
        }
    }
    if next.len() == 36 { addrs.push(next); }
    addrs
        .iter()
        .filter(|&x| x.len() == 36usize)
        .filter(|&x| x.chars().filter(|&c| c == 'X').count() == 0)
        .map(|x| x.clone())
        .collect()
}

fn part1(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    let mut mem: HashMap<usize,usize> = HashMap::new();
    let mut mask: Vec<(usize,usize)> = Vec::new();
    for cmd in input {
        match &cmd[0..4] {
            "mask" => {
                mask.clear();
                mask = build_mask(cmd.split_ascii_whitespace().last().unwrap());
            },
            "mem[" => {
                let addr: usize;
                let value: usize;
                scan!(cmd.bytes() => "mem[{}] = {}", addr, value);
                let mut value_b = base10_to_binary_vec(value);
                // Apply mask
                for (i,b) in &mask { value_b[*i] = *b; }
                let value = value_b.iter().map(|x| x.to_string()).collect::<Vec<_>>().concat();
                let value = usize::from_str_radix(&value,2).unwrap();
                mem.insert(addr,value);
            },
            other => panic!("Invalid program command: {}", other),
        }
    }

    let part1: usize = mem.iter().map(|(_,v)| v).sum();
    println!("Part 1: {}", part1); // 6317049172545

    Ok(())
}

fn part2(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    let mut mem: HashMap<usize,usize> = HashMap::new();
    let mut mask = String::new();
    for cmd in input {
        match &cmd[0..4] {
            "mask" => {
                mask.clear();
                mask = cmd.split_ascii_whitespace().last().unwrap().to_string();
            },
            "mem[" => {
                let addr: usize;
                let value: usize;
                scan!(cmd.bytes() => "mem[{}] = {}", addr, value);
                let addr = format!("{:036b}", addr).chars().collect::<String>();
                let renderedmask = apply_mask(&mask,&addr);          
                for addr in floating_addresses(&renderedmask,0) {
                    let addr = usize::from_str_radix(&addr,2).unwrap();
                    mem.insert(addr,value);
                }
            },
            other => panic!("Invalid program command: {}", other),
        }
    }

    let part2: usize = mem.iter().map(|(_,v)| v).sum();
    println!("Part 2: {}", part2); // 3434009980379

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    part1(&filename).unwrap();
    part2(&filename).unwrap();
}