use std::env;
use std::io::{self};
use std::collections::{VecDeque,HashSet};

const DBG: bool = false;

extern crate regex;
use regex::Regex;

fn score(cards_p1: VecDeque<usize>, cards_p2: VecDeque<usize>) -> (usize,usize) {
    let p1_count = cards_p1.len();
    let p2_count = cards_p2.len();

    let score1: usize = cards_p1.iter().enumerate().map(|(i,c)| (p1_count-i)*c).sum();
    let score2: usize = cards_p2.iter().enumerate().map(|(i,c)| (p2_count-i)*c).sum();

    let winner = if score1 > score2 { 1 } else if score2 > score1 { 2 } else { 0 };
    
    (winner,std::cmp::max(score1,score2)) // (winner,score)
}

fn combat(cards_p1: Vec<usize>, cards_p2: Vec<usize>) -> usize {
    let mut player1: VecDeque<usize> = VecDeque::from(cards_p1);
    let mut player2: VecDeque<usize> = VecDeque::from(cards_p2);

    let mut round = 0;
    'main_lp: loop {
        round += 1;
        if DBG {
            println!("-- Round {} --", round);
            println!("Player 1's deck: {:?}", player1);
            println!("Player 2's deck: {:?}", player2);
        }
        let player1_plays = player1.pop_front().unwrap();
        let player2_plays = player2.pop_front().unwrap();
        if DBG {
            println!("Player 1 plays: {}", player1_plays);
            println!("Player 2 plays: {}", player2_plays);
        }
        if player1_plays > player2_plays {
            player1.push_back(player1_plays);
            player1.push_back(player2_plays);
            if DBG { println!("Player 1 wins the round!"); }
        } else if player2_plays > player1_plays {
            player2.push_back(player2_plays);
            player2.push_back(player1_plays);
            if DBG { println!("Player 2 wins the round!"); }
        } else {
            panic!("Tie: {} {}", player1_plays, player2_plays);
        }
        if player1.len() == 0 || player2.len() == 0 { break 'main_lp; }
    }

    score(player1,player2).1
}

fn recursive_combat(cards_p1: VecDeque<usize>, cards_p2: VecDeque<usize>) -> (usize,usize) {
    let mut player1: VecDeque<usize> = VecDeque::from(cards_p1);
    let mut player2: VecDeque<usize> = VecDeque::from(cards_p2);

    let mut seen1: HashSet<VecDeque<usize>> = HashSet::new();
    let mut seen2: HashSet<VecDeque<usize>> = HashSet::new();
    let mut round = 0;
    'game_lp: loop {
        round += 1;
        if DBG {
            println!("-- Round {} --", round);
            println!("Player 1's deck: {:?}", player1);
            println!("Player 2's deck: {:?}", player2);
        }

        if seen1.contains(&player1) && seen2.contains(&player2) {
            if DBG { println!("Player 1 wins via HashSet"); }
            return (1,0); // player1 wins, no score
        }
        seen1.insert(player1.clone());
        seen2.insert(player2.clone());

        let player1_plays = player1.pop_front().unwrap();
        let player2_plays = player2.pop_front().unwrap();
        if DBG { println!("Player 1 plays: {}", player1_plays); }
        if DBG { println!("Player 2 plays: {}", player2_plays); }

        if player1.len() >= player1_plays && player2.len() >= player2_plays {
            let mut p1new = VecDeque::new();
            let mut p2new = VecDeque::new();
            for i in 0..player1_plays {
                p1new.push_back(**&player1.get(i).unwrap());
            }
            for i in 0..player2_plays {
                p2new.push_back(**&player2.get(i).unwrap());
            }

            let (winner,_) = recursive_combat(p1new,p2new);
            match winner {
                1 => {
                    player1.push_back(player1_plays);
                    player1.push_back(player2_plays);
                    if DBG { println!("Player 1 wins the round!"); }
                },
                2 => {
                    player2.push_back(player2_plays);
                    player2.push_back(player1_plays);
                    if DBG { println!("Player 2 wins the round!"); }
                },
                _ => panic!("Bad game"),
            }

        } else {

            if player1_plays > player2_plays {
                player1.push_back(player1_plays);
                player1.push_back(player2_plays);
                if DBG { println!("Player 1 wins the round!"); }
            } else if player2_plays > player1_plays {
                player2.push_back(player2_plays);
                player2.push_back(player1_plays);
                if DBG { println!("Player 2 wins the round!"); }
            } else {
                panic!("Tie: {} {}", player1_plays, player2_plays);
            }

        }

        if player1.len() == 0 || player2.len() == 0 { break 'game_lp; }
    }

    score(player1,player2)
}

fn part1(input: &str) -> io::Result<()> {
    let cards_input = std::fs::read_to_string(input).unwrap();
    let cards_input = cards_input
        .split("\n\n")
        .collect::<Vec<_>>();

    let re_num = Regex::new(r"^(\w+)$").unwrap();
    let cards_p1: Vec<usize> = cards_input[0].split("\n").filter(|&l| re_num.is_match(&l)).map(|x| x.parse::<usize>().unwrap()).collect();
    let cards_p2: Vec<usize> = cards_input[1].split("\n").filter(|&l| re_num.is_match(&l)).map(|x| x.parse::<usize>().unwrap()).collect();

    let part1 = combat(cards_p1,cards_p2);
    println!("Part 1: {}", part1); // 32179

    Ok(())
}

fn part2(input: &str) -> io::Result<()> {
    let cards_input = std::fs::read_to_string(input).unwrap();
    let cards_input = cards_input
        .split("\n\n")
        .collect::<Vec<_>>();

    let re_num = Regex::new(r"^(\w+)$").unwrap();
    let cards_p1: VecDeque<usize> = cards_input[0].split("\n").filter(|&l| re_num.is_match(&l)).map(|x| x.parse::<usize>().unwrap()).collect();
    let cards_p2: VecDeque<usize> = cards_input[1].split("\n").filter(|&l| re_num.is_match(&l)).map(|x| x.parse::<usize>().unwrap()).collect();

    let part2 = recursive_combat(cards_p1,cards_p2).1;
    println!("Part 2: {}", part2); // 30498

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    part1(&filename).unwrap();
    part2(&filename).unwrap();
}