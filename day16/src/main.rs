use std::env;
use std::io::{prelude::*, BufReader};
use std::fs::File;
use std::collections::HashMap;
use std::ops::RangeInclusive;

extern crate regex;
use regex::Regex;

#[macro_use]
extern crate text_io;

#[derive(Debug, Clone)]
struct TicketField {
    name: String,
    range1: RangeInclusive<usize>,
    range2: RangeInclusive<usize>,
    possible_pos: Vec<bool>,
}
impl From<&String> for TicketField {
    fn from(input: &String) -> Self {
        // departure station: 28-106 or 130-969
        let name: String;
        let mut range1: (usize,usize) = (0,0);
        let mut range2: (usize,usize) = (0,0);
        scan!(input.bytes() => "{}: {}-{} or {}-{}", name, range1.0, range1.1, range2.0, range2.1);
        Self {
            name: name,
            range1: range1.0..=range1.1,
            range2: range2.0..=range2.1,
            possible_pos: Vec::new(),
        }
    }
}
impl TicketField {
    pub fn validate(&self, value: usize) -> bool {
        self.range1.contains(&value) || self.range2.contains(&value)
    }
}

#[derive(Debug, Clone)]
struct Ticket {
    fields: Vec<usize>,
    valid: bool,
}
impl From<&String> for Ticket {
    fn from(input: &String) -> Self {
        Self {
            fields: input.split(",").map(str::parse::<usize>).map(Result::unwrap).collect::<Vec<_>>(),
            valid: true,
        }
    }
}

fn day16(input: &str) {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    // Process input
    let mut fields: Vec<TicketField> = Vec::new();
    let mut tickets: Vec<Ticket> = Vec::new();

    let re_field = Regex::new(r"^[\w ]+: [\d]+\-[\d]+ or [\d]+\-[\d]+$").unwrap();
    let re_ticket = Regex::new(r"^([\d]+,)+[\d]+$").unwrap();
    for line in input {
        if re_field.is_match(&line) {
            fields.push(TicketField::from(&line));
        } else if re_ticket.is_match(&line) {
            tickets.push(Ticket::from(&line));
        }
    }

    // Part 1
    let mut part1 = 0;
    for ticket in &mut tickets {
        for val in &ticket.fields {
            if fields.iter().map(|f| f.validate(*val)).filter(|&f| f == true).count() == 0 {
                part1 += val;
                ticket.valid = false;
            }
        }
    }
    println!("Part 1: {}", part1); // 26009

    // Part 2
    let tickets = tickets.iter().filter(|t| t.valid).collect::<Vec<_>>();
    let num_fields = fields.len();
    let my_ticket = tickets.get(0).unwrap();

    // Build array of bools to track possible field positions
    for field in &mut fields { field.possible_pos = vec![true; num_fields]; }

    // Rule out certain field positions
    for field in &mut fields {
        for ticket in &tickets {
            for (ix,val) in ticket.fields.iter().enumerate() {
                if !field.validate(*val) {
                    field.possible_pos[ix] = false;
                }
            }
        }
    }

    // Narrow down one field at a time
    let mut order: HashMap<usize,String> = HashMap::new();
    while order.len() != num_fields {
        for i in 0..num_fields {
            if fields[i].possible_pos.iter().filter(|&x| *x == true).count() != 1 { continue; }
            let ix = fields[i].possible_pos.iter().position(|&x| x == true).unwrap();
            order.insert(ix, fields[i].name.clone());
            for f_other in &mut fields { f_other.possible_pos[ix] = false; }
            
        }
    }

    // Calculate part 2 answer
    let part2 = my_ticket
        .fields
        .iter()
        .enumerate()
        .map(|(i,x)| 
            match order.get(&i).unwrap().split_ascii_whitespace().next().unwrap() {
                "departure" => x,
                _ => &1usize,
            })
        .product::<usize>();

    println!("Part 2: {}", part2); // 589685618167

}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day16(&filename);
}