use std::env;
use std::io::{prelude::*, BufReader};
use std::fs::File;

extern crate itertools;
use itertools::Itertools;

fn validator_part1(input: &Vec<i64>, predicate: usize) -> i64 {
    let mut ans: i64 = 0;
    for window in input.windows(predicate+1) {
        let target = window.iter().last().unwrap();
        if window
            .iter()
            .combinations(2)
            .filter(|pair| pair[0]+pair[1] == *target && pair[0] != pair[1])
            .count() < 1 {
            ans = *target;
            break;
        }
    }
    ans
}

fn validator_part2(input: &Vec<i64>, target: i64) -> i64 {
    let mut size: usize = 2;
    loop {
        for window in input.windows(size) {
            if window.iter().sum::<i64>() == target {
                return window.iter().min().unwrap() + window.iter().max().unwrap();
            }
        }
        size += 1;
    }
}

fn day09(input: &str) {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);
    let input: Vec<_> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    let stream: Vec<_> = input.iter().map(|x| x.parse::<i64>()).map(Result::unwrap).collect();

    // Part 1
    let part1 = validator_part1(&stream,25);
    println!("Part 1: {}", part1); // 14144619
    
    // Part 2
    let part2 = validator_part2(&stream,part1);
    println!("Part 2: {}", part2); // 1766397
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day09(&filename);
}