use std::env;
use std::io::{self};
use std::collections::HashMap;

extern crate regex;
use regex::Regex;

#[derive(Debug, Clone)]
enum Rule {
    Char(char),
    MatchOne(usize),
    MatchTwo(usize,usize),
    MatchThree(usize,usize,usize),
    MatchOneOrOne(usize,usize),
    MatchTwoOrTwo(usize,usize,usize,usize),
    MatchOneOrTwo(usize,usize,usize),
    MatchTwoOrThree(usize,usize,usize,usize,usize),
}
impl Rule {
    pub fn as_regex(&self, rules: &HashMap<usize,Rule>) -> String {
        match &self {
            Self::Char(a) => format!("({})",a),
            Self::MatchOne(a) => {
                let a = rules.get(a).unwrap().as_regex(&rules);
                format!("({})",a)
            },
            Self::MatchTwo(a1,a2) => {
                let a1 = rules.get(a1).unwrap().as_regex(&rules);
                let a2 = rules.get(a2).unwrap().as_regex(&rules);
                format!("({}{})",a1,a2)
            },
            Self::MatchThree(a1,a2,a3) => {
                let a1 = rules.get(a1).unwrap().as_regex(&rules);
                let a2 = rules.get(a2).unwrap().as_regex(&rules);
                let a3 = rules.get(a3).unwrap().as_regex(&rules);
                format!("({}{}{})",a1,a2,a3)
            },
            Self::MatchOneOrOne(a1,b1) => {
                let a1 = rules.get(a1).unwrap().as_regex(&rules);
                let b1 = rules.get(b1).unwrap().as_regex(&rules);
                format!("({}|{})",a1,b1)
            },
            Self::MatchTwoOrTwo(a1,a2,b1,b2) => {
                let a1 = rules.get(a1).unwrap().as_regex(&rules);
                let a2 = rules.get(a2).unwrap().as_regex(&rules);
                let b1 = rules.get(b1).unwrap().as_regex(&rules);
                let b2 = rules.get(b2).unwrap().as_regex(&rules);
                format!("({}{}|{}{})",a1,a2,b1,b2)
            },

            Self::MatchOneOrTwo(a1,b1,b2) => {
                let a1 = rules.get(a1).unwrap().as_regex(&rules);
                let b1 = rules.get(b1).unwrap().as_regex(&rules);
                let b2 = rules.get(b2).unwrap().as_regex(&rules);
                format!("({}|{}{})",a1,b1,b2)
            },

            Self::MatchTwoOrThree(a1,a2,b1,b2,b3) => {
                let a1 = rules.get(a1).unwrap().as_regex(&rules);
                let a2 = rules.get(a2).unwrap().as_regex(&rules);
                let b1 = rules.get(b1).unwrap().as_regex(&rules);
                let b2 = rules.get(b2).unwrap().as_regex(&rules);
                let b3 = rules.get(b3).unwrap().as_regex(&rules);
                format!("({}{}|{}{}{})",a1,a2,b1,b2,b3)
            },
        }
    }
    pub fn is_match(&self, message: &str, rules: &HashMap<usize,Rule>) -> bool {
        let re = format!("^{}$", &self.as_regex(&rules));
        let re = pcre2::bytes::Regex::new(&re).unwrap();
        re.is_match(message.as_bytes()).unwrap()
    }
}
impl From<&str> for Rule {
    fn from(input: &str) -> Self {
        use Rule::*;
        let regexes = [
            r"([a-z])",                             // 0
            r"^ (\d+)$",                            // 1
            r"^ (\d+) (\d+)$",                      // 2
            r"^ (\d+) (\d+) (\d+)$",                // 3
            r"^ (\d+) \| (\d+)$",                   // 4
            r"^ (\d+) (\d+) \| (\d+) (\d+)$",       // 5
            r"^ (\d+) \| (\d+) (\d+)$",             // 6
            r"^ (\d+) (\d+) \| (\d+) (\d+) (\d+)$", // 7
        ];
        let set = regex::RegexSet::new(&regexes).unwrap();

        let rix = set.matches(&input).into_iter().next().unwrap();
        let re = Regex::new(regexes[rix]).unwrap();
        let caps = re.captures(&input).unwrap();
        match rix {
            0 => Char(caps[1].parse().unwrap()),
            1 => MatchOne(caps[1].parse().unwrap()),
            2 => MatchTwo(caps[1].parse().unwrap(),caps[2].parse().unwrap()),
            3 => MatchThree(caps[1].parse().unwrap(),caps[2].parse().unwrap(),caps[3].parse().unwrap()),
            4 => MatchOneOrOne(caps[1].parse().unwrap(),caps[2].parse().unwrap()),
            5 => MatchTwoOrTwo(caps[1].parse().unwrap(),caps[2].parse().unwrap(),caps[3].parse().unwrap(),caps[4].parse().unwrap()),
            6 => MatchOneOrTwo(caps[1].parse().unwrap(),caps[2].parse().unwrap(),caps[3].parse().unwrap()),          
            7 => MatchTwoOrThree(caps[1].parse().unwrap(),caps[2].parse().unwrap(),caps[3].parse().unwrap(),caps[4].parse().unwrap(),caps[5].parse().unwrap()),
            _ => panic!("Failed to match regex: {}", input),
        }
    }
}

fn day19(input: &str) -> io::Result<()> {
    
    // Read and collect input
    let input = std::fs::read_to_string(input).expect("Input file not found.");
    let rules =    input.split("\n\n").nth(0).unwrap();
    let messages = input.split("\n\n").nth(1).unwrap();
    let messages = messages.split("\n").collect::<Vec<_>>();

    // Build rules
    let rules: HashMap<usize,Rule> = rules
        .lines()
        .map(|line| {
            let parts = line.split(":").collect::<Vec<_>>();
            let rule_id = parts[0].parse::<usize>().unwrap();
            let rule = Rule::from(parts[1]);
            (rule_id,rule)
        })
        .collect();
    let rule0 = rules.get(&0).unwrap();
    
    // Part 1
    let part1 = messages.iter().filter(|msg| rule0.is_match(msg,&rules)).count();
    println!("Part 1: {}", part1); // Part 1: 299

    // Part 2
    let mut rules = rules.clone();
    rules.insert(8, Rule::MatchOneOrTwo(42,42,8));
    rules.insert(11,Rule::MatchTwoOrThree(42,31,42,11,41));
    
    let re2 = Box::new(format!("^{}$", Box::new(&rule0.as_regex(&rules))));
    let re2 = pcre2::bytes::Regex::new(&re2).unwrap();
    let part2 = messages.iter().filter(|msg| re2.is_match(msg.as_bytes()).unwrap()).count();
    //let re2 = Regex::new(&re2).unwrap();
    //let part2 = messages.iter().filter(|msg| re2.is_match(msg)).count();
    println!("Part 2: {}", part2); // Part 2: 414

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day19(&filename).unwrap();
}
