use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::HashSet;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
struct Tree {
    x: i64,
    y: i64,
}
impl Tree {
    pub fn new(x: i64, y: i64) -> Self {
        Self { x, y }
    }
}

fn tree_collisions(trees: &HashSet<Tree>, width: i64, slope: (i64,i64)) -> usize {
    trees
        .iter()
        .filter(|p| p.y % slope.1 == 0)
        .filter(|p| p.x == (slope.0*p.y / slope.1) % (width+1))
        .count()
}

fn day03(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    // Build map of trees
    let mut width: i64 = 0;
    let mut trees: HashSet<Tree> = HashSet::new();
    for (y,line) in reader.lines().enumerate() {
        for (x,c) in line.unwrap().chars().enumerate() {
            let (x,y) = (x as i64, y as i64);
            width = std::cmp::max(width,x);
            if c == '#' { trees.insert(Tree::new(x,y)); }
        }
    }

    let slopes = vec![(1,1), (3,1), (5,1), (7,1), (1,2)];

    // Part 1
    let part1 = tree_collisions(&trees,width,slopes[1]);
    println!("Part 1: {}", part1); // 247

    let part2: usize = slopes
        .iter()
        .map(|s| tree_collisions(&trees,width,*s))
        .product();
    println!("Part 2: {}", part2); // 2983070376

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day03(&filename).unwrap();
}