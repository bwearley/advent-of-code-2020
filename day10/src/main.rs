use std::env;
use std::io::{prelude::*, BufReader};
use std::fs::File;

#[macro_use] extern crate lazy_static;
lazy_static! {
    static ref ADAPTERS: Vec<i64> = {
        let args: Vec<String> = env::args().collect();
        let file = File::open(&args[1]).expect("Input file not found.");
        let reader = BufReader::new(file);
        let input: Vec<_> = match reader.lines().collect() {
            Err(err) => panic!("Unknown error reading input: {}", err),
            Ok(result) => result,
        };
    
        let mut adapters: Vec<_> = input.iter().map(|x| x.parse::<i64>().unwrap()).collect();
        adapters.sort();
        adapters.insert(0,0);
        adapters.push(adapters.iter().max().unwrap()+3);
        adapters
    };
}

use cached::proc_macro::cached;
#[cached]
fn num_configurations(adapters: &'static [i64], from: i64, to: i64) -> i64 {
    let mut sum = 0;
    for next in &[from+1, from+2, from+3] {
        if next == &to { 
            sum += 1;
        } else if adapters.iter().filter(|x| x == &next).count() == 1 {
            sum += num_configurations(&adapters, *next, to);
        }
    }
    sum
}

fn main() {

    // Part 1
    let diffs1jolt = &ADAPTERS.windows(2)
        .filter(|pair| pair[1]-pair[0] == 1)
        .count();
    let diffs3jolt = &ADAPTERS.windows(2)
        .filter(|pair| pair[1]-pair[0] == 3)
        .count();
    println!("Part 1: {}", diffs1jolt * diffs3jolt); // 1836

    // Part 2
    let plug = &ADAPTERS.iter().max().unwrap();
    println!("Part 2: {}", num_configurations(&ADAPTERS, 0, **plug)); // 43406276662336
}