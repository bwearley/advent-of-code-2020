use std::env;
use std::io;
use std::collections::{HashMap,VecDeque};

#[macro_use]
extern crate text_io;

const DIM1: usize = 10;
const DIM2: usize = 9;

#[derive(Debug,Clone)]
struct Tile {
    id: usize,
    img: HashMap<(usize,usize),bool>,
    edges: Vec<String>,
    neighbors: Vec<usize>,
    drawn: bool,
}
impl From<&str> for Tile {
    fn from(input: &str) -> Self {
        let lines = input.lines().collect::<Vec<_>>();
        let id: usize;
        scan!(lines.get(0).unwrap().bytes() => "Tile {}:", id);
        let mut map: HashMap<(usize,usize),bool> = HashMap::new();
        for (y,line) in lines[1..].iter().enumerate() {
            for (x,ch) in line.chars().enumerate() {
                match ch {
                    '#' => { map.insert((x,y),true); },
                    '.' => { map.insert((x,y),false); },
                    other => panic!("Unknown square: {}", other),
                }
            }
        }
        // Get edges
        let mut self_ = Self { id: id, img: map, edges: Vec::new(), neighbors: Vec::new(), drawn: false };
        self_.make_edges();
        self_
    }
}
impl Tile {
    pub fn make_edges(&mut self) {
        self.edges.clear();
        let mut edges: Vec<String> = vec![self.edge_top(),self.edge_bottom(),self.edge_left(),self.edge_right()];
        edges.push(self.edge_top().reversed());
        edges.push(self.edge_bottom().reversed());
        edges.push(self.edge_left().reversed());
        edges.push(self.edge_right().reversed());
        self.edges = edges;
    }
    pub fn edge_top(&self) -> String {
        (0..DIM1).map(|x| bool_char(self.img.get(&(x,0)).unwrap())).collect::<String>()
    }
    pub fn edge_bottom(&self) -> String {
        (0..DIM1).map(|x| bool_char(self.img.get(&(x,DIM1-1)).unwrap())).collect::<String>()
    }
    pub fn edge_left(&self) -> String {
        (0..DIM1).map(|y| bool_char(self.img.get(&(0,y)).unwrap())).collect::<String>()
    }
    pub fn edge_right(&self) -> String {
        (0..DIM1).map(|y| bool_char(self.img.get(&(DIM1-1,y)).unwrap())).collect::<String>()
    }
    pub fn flip_lr(&mut self) {
        let img_copy = self.img.clone();
        for y in 0..DIM1 {
            for x in 0..DIM1 {
                let copy = img_copy.get(&(x,y)).unwrap();
                self.img.insert((DIM1-1-x,y),*copy);
            }
        }
        self.make_edges();
    }
    pub fn flip_tb(&mut self) {
        let img_copy = self.img.clone();
        for y in 0..DIM1 {
            for x in 0..DIM1 {
                let copy = img_copy.get(&(x,y)).unwrap();
                self.img.insert((x,DIM1-1-y),*copy);
            }
        }
        self.make_edges();
    }
    pub fn rotate_ccw(&mut self) {
        let img_copy = self.img.clone();
        for y in 0..DIM1 {
            for x in 0..DIM1 {
                self.img.insert((y,x),*img_copy.get(&(DIM1-1-x,y)).unwrap());
            }
        }
        self.make_edges();
    }
}
trait Reversed {
    fn reversed(&self) -> Self;
}
impl Reversed for String {
    fn reversed(&self) -> String {
        self.chars().rev().collect::<String>()
    }
}

fn map_extents<T>(map: &HashMap<(i64,i64),T>) -> (i64,i64,i64,i64) {
    let xmin = &map.keys().map(|&(x,_)| x).min().unwrap();
    let ymin = &map.keys().map(|&(_,y)| y).min().unwrap();
    let xmax = &map.keys().map(|&(x,_)| x).max().unwrap();
    let ymax = &map.keys().map(|&(_,y)| y).max().unwrap();
    (*xmin,*ymin,*xmax,*ymax)
}

fn bool_char(b: &bool) -> String {
    match b {
        true =>  "#".to_string(),
        false => ".".to_string(),
    }
}

fn day20(input: &str) -> io::Result<()> {
    let mut tiles = std::fs::read_to_string(input)
        .unwrap()
        .split("\n\n")
        .map(Tile::from)
        .collect::<Vec<_>>();

    let num_tiles = tiles.len();

    // Find neighbors
    for tile in 0..num_tiles {
        for other in 0..num_tiles {
            if tile == other { continue; }
            let mut matches = false;
            'outer: for edge in &tiles[tile].edges {
                for other_edge in &tiles[other].edges {
                    if edge == other_edge {
                        matches = true;
                        break 'outer;
                    }
                }
            }
            if matches {
                tiles[tile].neighbors.push(other);
                tiles[other].neighbors.push(tile);
            }
        }
    }

    // Dedup neighbors
    for tile in 0..num_tiles {
        tiles[tile].neighbors.sort();
        tiles[tile].neighbors.dedup();
    }

    // Part 1
    let part1: usize = tiles
        .iter()
        .filter(|tile| tile.neighbors.len() == 2)
        .map(|tile| tile.id)
        .product();
    println!("Part 1: {}", part1); // 27803643063307
  
    // This rotation was previously mixed in with other code
    // Now stuck with it since the rest of the built-in rotations
    // hinge on this.
    tiles[0].rotate_ccw();

    // Starting with tile 0, find all neighbors
    // place and orient them as appropriate
    let mut grid: HashMap<(i64,i64),Tile> = HashMap::new();
    let mut q: VecDeque<(usize,i64,i64)> = VecDeque::new();
    tiles[0].drawn = true;
    grid.insert((0,0),tiles[0].clone());
    q.push_back((0,0,0)); // (tile_id, x, y)
    while q.len() > 0 {
        let (t_id,x,y) = q.pop_front().unwrap();
        'neighbors: for neighbor_id in &tiles[t_id].neighbors.clone() {
            if tiles[*neighbor_id].drawn { continue 'neighbors; }
            
            // Edges of current tile (only actual edges, not reversed)
            for (eid,edge) in tiles[t_id].clone().edges[0..4].into_iter().enumerate() {
                
                // Match against all edges of neighboring tile, including reversed
                for (eid_other,other_edge) in tiles[*neighbor_id].clone().edges.iter().enumerate() {

                    // Found match
                    if edge == other_edge {
                        let newxy: (i64,i64);
                        match (eid,eid_other) {
                            (0,0) => { // top - matches top
                                newxy = (x,y-1);
                                tiles[*neighbor_id].flip_tb();
                            }, 
                            (0,1) => { // top - matches bottom
                                newxy = (x,y-1);
                            },
                            (0,2) => { // top - matches left
                                newxy = (x,y-1);
                                tiles[*neighbor_id].rotate_ccw();
                            },
                            (0,3) => { // top - matches right
                                newxy = (x,y-1);
                                tiles[*neighbor_id].flip_lr();
                                tiles[*neighbor_id].rotate_ccw();
                            },
                            (0,4) => { // top - matches top reversed
                                newxy = (x,y-1);
                                tiles[*neighbor_id].rotate_ccw();
                                tiles[*neighbor_id].rotate_ccw();
                            }, 
                            (0,5) => { // top - matches bottom reversed
                                newxy = (x,y-1);
                                tiles[*neighbor_id].flip_lr();
                            },
                            (0,6) => { // top - matches left reversed
                                newxy = (x,y-1);
                                tiles[*neighbor_id].rotate_ccw();
                                tiles[*neighbor_id].flip_lr();
                            },
                            (0,7) => { // top - matches right reversed
                                newxy = (x,y-1);
                                tiles[*neighbor_id].rotate_ccw();
                                tiles[*neighbor_id].rotate_ccw();
                                tiles[*neighbor_id].rotate_ccw();
                            },
                            (1,0) => { // bottom - matches top
                                newxy = (x,y+1);
                            },
                            (1,1) => { // bottom - matches bottom
                                newxy = (x,y+1);
                                tiles[*neighbor_id].flip_tb();
                            },
                            (1,2) => { // bottom - matches left
                                newxy = (x,y+1);
                                tiles[*neighbor_id].rotate_ccw();
                                tiles[*neighbor_id].rotate_ccw();
                                tiles[*neighbor_id].rotate_ccw();
                                tiles[*neighbor_id].flip_lr();
                            },
                            (1,3) => { // bottom - matches right
                                newxy = (x,y+1);
                                tiles[*neighbor_id].rotate_ccw();
                            },
                            (1,4) => { // bottom - matches top reversed
                                newxy = (x,y+1);
                                tiles[*neighbor_id].flip_lr();
                            },
                            (1,5) => { // bottom - matches bottom reversed
                                newxy = (x,y+1);
                                tiles[*neighbor_id].rotate_ccw();
                                tiles[*neighbor_id].rotate_ccw();
                            },
                            (1,6) => { // bottom - matches left reversed
                                newxy = (x,y+1);
                                tiles[*neighbor_id].rotate_ccw();
                                tiles[*neighbor_id].rotate_ccw();
                                tiles[*neighbor_id].rotate_ccw();
                            },
                            (1,7) => { // bottom - matches right reversed
                                newxy = (x,y+1);
                                tiles[*neighbor_id].rotate_ccw();
                                tiles[*neighbor_id].flip_lr();
                            },
                            (2,0) => { // left - matches top
                                newxy = (x-1,y);
                                tiles[*neighbor_id].rotate_ccw();
                                tiles[*neighbor_id].rotate_ccw();
                                tiles[*neighbor_id].rotate_ccw();
                            },
                            (2,1) => { // left - matches bottom
                                newxy = (x-1,y);
                                tiles[*neighbor_id].rotate_ccw();
                                tiles[*neighbor_id].flip_tb();
                            },
                            (2,2) => { // left - matches left
                                newxy = (x-1,y);
                                tiles[*neighbor_id].flip_lr();
                            },
                            (2,3) => { // left - matches right
                                newxy = (x-1,y);
                            },
                            (2,4) => { // left - matches top reversed
                                newxy = (x-1,y);
                                //tiles[*neighbor_id].rotate_ccw();
                                //tiles[*neighbor_id].rotate_ccw();
                                //tiles[*neighbor_id].rotate_ccw();
                                tiles[*neighbor_id].flip_tb();
                                tiles[*neighbor_id].rotate_ccw();
                            },
                            (2,5) => { // left - matches bottom reversed
                                newxy = (x-1,y);
                                tiles[*neighbor_id].rotate_ccw();
                            },
                            (2,6) => { // left - matches left reversed
                                newxy = (x-1,y);
                                tiles[*neighbor_id].rotate_ccw();
                                tiles[*neighbor_id].rotate_ccw();
                            },
                            (2,7) => { // left - matches right reversed
                                newxy = (x-1,y);
                                tiles[*neighbor_id].flip_tb();
                            },
                            (3,0) => { // right - matches top
                                newxy = (x+1,y);
                                tiles[*neighbor_id].rotate_ccw();
                                tiles[*neighbor_id].flip_tb();
                            },
                            (3,1) => { // right - matches bottom
                                newxy = (x+1,y);
                                tiles[*neighbor_id].rotate_ccw();
                                tiles[*neighbor_id].rotate_ccw();
                                tiles[*neighbor_id].rotate_ccw();
                            },
                            (3,2) => { // right - matches left
                                newxy = (x+1,y);
                            },
                            (3,3) => { // right - matches right
                                newxy = (x+1,y);
                                tiles[*neighbor_id].flip_lr();
                            },
                            (3,4) => { // right - matches top reversed
                                newxy = (x+1,y);
                                tiles[*neighbor_id].rotate_ccw();
                            },
                            (3,5) => { // right - matches bottom reversed
                                newxy = (x+1,y);                               
                                tiles[*neighbor_id].flip_tb();
                                tiles[*neighbor_id].rotate_ccw();
                            },
                            (3,6) => { // right - matches left reversed
                                newxy = (x+1,y);
                                tiles[*neighbor_id].flip_tb();
                            },
                            (3,7) => { // right - matches right reversed
                                newxy = (x+1,y);
                                tiles[*neighbor_id].rotate_ccw();
                                tiles[*neighbor_id].rotate_ccw();
                            },
                            (_,_) => panic!("Bad combination!"),
                        }
                        tiles[*neighbor_id].drawn = true;
                        grid.insert(newxy,tiles[*neighbor_id].clone());
                        q.push_back((*neighbor_id,newxy.0,newxy.1));
                    }
                }
            }
        }
    }

    // Print grid of tile IDs
    let (xmin,ymin,xmax,ymax) = map_extents(&grid);
    for y in ymin..=ymax {
        for x in xmin..=xmax {
            print!(" {} ", grid.get(&(x,y)).unwrap().id);
        }
        println!();
    }
    println!();

    // Build actual image (and print with tile IDs)
    let mut img: HashMap<(i64,i64),bool> = HashMap::new();
    let mut newx = 0;
    let mut newy = 0;
    for y in ymin..=ymax {
        //for y2 in 0..DIM1 {
        for y2 in 1..DIM2 {
            for x in xmin..=xmax {
                print!("  ");
                let t = grid.get(&(x,y)).unwrap();
                print!("{} ",t.id);
              //for x2 in 0..DIM1 {
                for x2 in 1..DIM2 {
                    print!("{}", bool_char(t.img.get(&(x2,y2)).unwrap()));
                    img.insert((newx,newy),*t.img.get(&(x2,y2)).unwrap());
                    newx += 1;
                }
            }
            newx = 0;
            newy += 1;
            println!();
        }
        println!();
    }

    // Display pre-oriented image
    println!("Final Image:");
    let (xmin,ymin,xmax,ymax) = map_extents(&img);
    println!("x=({},{}), y=({},{})",xmin,xmax,ymin,ymax);
    for y in ymin..=ymax {
        for x in xmin..=xmax {
            print!("{}", bool_char(img.get(&(x,y)).unwrap()));
        }
        println!();
    }

    // Display image in new non-final orientation
    println!();
    println!("Final Image rotated:");
    rotate_map_ccw(&mut img);
    flip_map_lr(&mut img);
    let (xmin,ymin,xmax,ymax) = map_extents(&img);
    println!("x=({},{}), y=({},{})",xmin,xmax,ymin,ymax);
    for y in ymin..=ymax {
        for x in xmin..=xmax {
            print!("{}", bool_char(img.get(&(x,y)).unwrap()));
        }
        println!();
    }

    // Orient map to find monsters
    rotate_map_ccw(&mut img);
    rotate_map_ccw(&mut img);
    rotate_map_ccw(&mut img);
    flip_map_lr(&mut img);

    // Count monsters
    let mut num_monsters = 0;
    const MONSTER_NUM: usize = 15;
    for y in 3..ymax {
        for x in 0..=xmax-19 {
            let monster_pts = [
                (x,y),(x+1,y+1),(x+4,y+1),(x+5,y),(x+6,y),(x+7,y+1),(x+10,y+1),
                (x+11,y),(x+12,y),(x+13,y+1),(x+16,y+1),(x+17,y),(x+18,y),(x+19,y),
                (x+18,y-1)
            ];
            if monster_pts.iter().map(|pt| img.get(&pt).unwrap()).filter(|&t| *t).count() == MONSTER_NUM {
                num_monsters += 1;
            }
        }
    }

    // Count non-monster
    let hashes = img.iter().filter(|&(_,v)| *v).count();
    println!("Hashes: {}", hashes);
    println!("Monsters: {}", num_monsters);

    // Part 2
    let part2 = hashes as i64 - (num_monsters*MONSTER_NUM as i64);
    println!("Part 2: {}", part2); // 1644

    Ok(())
}

fn flip_map_lr<T: Clone>(map: &mut HashMap<(i64,i64),T>) {
    let map_copy = map.clone();
    let (xmin,ymin,xmax,ymax) = map_extents(&map);
    for y in ymin..=ymax {
        for x in xmin..=xmax {
            map.insert((xmax-x,y),map_copy.get(&(x,y)).unwrap().clone());
        }
    }
}
fn flip_map_tb<T: Clone>(map: &mut HashMap<(i64,i64),T>) {
    let map_copy = map.clone();
    let (xmin,ymin,xmax,ymax) = map_extents(&map);
    for y in ymin..=ymax {
        for x in xmin..=xmax {
            map.insert((x,ymax-y),map_copy.get(&(x,y)).unwrap().clone());
        }
    }
}
fn rotate_map_ccw<T: Clone>(map: &mut HashMap<(i64,i64),T>) {
    let map_copy = map.clone();
    let (xmin,ymin,xmax,ymax) = map_extents(&map);
    for y in ymin..=ymax {
        for x in xmin..=xmax {
            map.insert((y,x),map_copy.get(&(xmax-x,y)).unwrap().clone());
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day20(&filename).unwrap();
}
