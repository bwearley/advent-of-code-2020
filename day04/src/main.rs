use std::env;
use std::io;

extern crate regex;
use regex::Regex;

#[derive(Default)]
struct Passport {
    byr: Option<String>,
    iyr: Option<String>,
    eyr: Option<String>,
    hgt: Option<String>,
    hcl: Option<String>,
    ecl: Option<String>,
    pid: Option<String>,
    cid: Option<String>,
}
impl Passport {
    pub fn is_valid_part1(&self) -> bool {
        self.byr.is_some() &&
        self.iyr.is_some() &&
        self.eyr.is_some() &&
        self.hgt.is_some() &&
        self.hcl.is_some() &&
        self.ecl.is_some() &&
        self.pid.is_some()
    }
    pub fn is_valid_part2(&self) -> bool {
        self.is_valid_part1() &&
        self.byr_valid() &&
        self.iyr_valid() &&
        self.eyr_valid() &&
        self.hgt_valid() &&
        self.hcl_valid() &&
        self.ecl_valid() &&
        self.pid_valid()
    }
    pub fn byr_valid(&self) -> bool {
        let byr: i64 = self.byr.as_ref().unwrap().parse().unwrap_or(-1);
        (1920..=2002).contains(&byr)
    }
    pub fn iyr_valid(&self) -> bool {
        let iyr: i64 = self.iyr.as_ref().unwrap().parse().unwrap_or(-1);
        (2010..=2020).contains(&iyr)
    }
    pub fn eyr_valid(&self) -> bool {
        let eyr: i64 = self.eyr.as_ref().unwrap().parse().unwrap_or(-1);
        (2020..=2030).contains(&eyr)
    }
    pub fn hgt_valid(&self) -> bool {
        let re = Regex::new(r"^(\d+)(\w+)$").unwrap();
        if !re.is_match(&self.hgt.as_ref().unwrap()) { return false; }
        let matches = re.captures(&self.hgt.as_ref().unwrap()).unwrap();
        let hgt_num: i64     = matches[1].parse().unwrap();
        let hgt_unit: String = matches[2].parse().unwrap();
        match hgt_unit.as_str() {
            "cm" => (150..=193).contains(&hgt_num),
            "in" => (59..=76).contains(&hgt_num),
            _ =>    false,
        }
    }
    pub fn hcl_valid(&self) -> bool {
        let re = Regex::new(r"^\#[a-f0-9]{6}$").unwrap();
        re.is_match(&self.hcl.as_ref().unwrap())
    }
    pub fn ecl_valid(&self) -> bool {
        let re = Regex::new(r"^(amb|blu|brn|gry|grn|hzl|oth)$").unwrap();
        re.is_match(&self.ecl.as_ref().unwrap())
    }
    pub fn pid_valid(&self) -> bool {
        let re = Regex::new(r"^\d{9}$").unwrap();
        re.is_match(&self.pid.as_ref().unwrap())
    }
}

fn day04(input: &str) -> io::Result<()> {
    let passports = std::fs::read_to_string(input)
        .unwrap()
        .split("\n\n")
        .map(|vline| {
            let mut passport = Passport::default();
            for part in vline.split_ascii_whitespace() {
                let mut data = part.split(":");
                match data.next().unwrap() {
                    "byr" => passport.byr = Some(data.next().unwrap().to_string()),
                    "iyr" => passport.iyr = Some(data.next().unwrap().to_string()),
                    "eyr" => passport.eyr = Some(data.next().unwrap().to_string()),
                    "hgt" => passport.hgt = Some(data.next().unwrap().to_string()),
                    "hcl" => passport.hcl = Some(data.next().unwrap().to_string()),
                    "ecl" => passport.ecl = Some(data.next().unwrap().to_string()),
                    "pid" => passport.pid = Some(data.next().unwrap().to_string()),
                    "cid" => passport.cid = Some(data.next().unwrap().to_string()),
                    other  => panic!("Unknown data type: {}", other),
                }
            }
            passport
        })
        .collect::<Vec<Passport>>();

    // Part 1
    let part1 = passports.iter().filter(|x| x.is_valid_part1()).count();
    println!("Part 1: {}", part1); // 233

    // Part 2
    let part2 = passports.iter().filter(|x| x.is_valid_part2()).count();
    println!("Part 2: {}", part2); // 111

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day04(&filename).unwrap();
}