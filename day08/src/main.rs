use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::time::Instant;

use assemblyvm::assemblyvm::AssemblyVM;
extern crate assemblyvm;

fn day08(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    // Initialize & Configure VM
    let mut vm = AssemblyVM::new(input);
    vm.break_on_loop = true;

    // Part 1
    vm.run();
    println!("Part 1: {}", vm.accum); // 1087
    vm.reset();

    // Part 2
    for i in 0..vm.code.len() {
        match vm.code[i].instr.as_ref() {
            "acc" => continue,
            "jmp" => vm.code[i].instr = "nop".to_string(),
            "nop" => vm.code[i].instr = "jmp".to_string(),
            other => unreachable!("Fail: {}", other),
        };
        vm.run();
        if !vm.loop_detected { break; }
        vm.reset();
    }
    println!("Part 2: {}", vm.accum); // 780
    
    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let start = Instant::now();
    day08(&filename).unwrap();
    println!("Total time: {:?}", Instant::now() - start);
}