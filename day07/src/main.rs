use std::env;
use std::io::{prelude::*, BufReader};
use std::fs::File;

extern crate regex;
use regex::Regex;

extern crate itertools;
use itertools::Itertools;

struct Regulation {
    color: String,
    contains: Vec<Bag>,
}
impl From<&String> for Regulation {
    fn from(input: &String) -> Self {
        let parts: Vec<_> = input.split(" contain ").collect();

        let re = Regex::new(r"^(\w+ \w+) bags$").unwrap();
        let matches = re.captures(&parts[0]).unwrap();
        let color: String = matches[1].parse().unwrap();

        let contains: Vec<Bag> = match parts[1] {
            "no other bags." => Vec::new(),
            _ => parts[1].split(",").map(Bag::from).collect(),
        };

        Self { color, contains }
    }
}

struct Bag {
    color: String,
    number: usize,
}
impl From<&str> for Bag {
    fn from(input: &str) -> Self {
        let re = Regex::new(r"(\d+) (\w+ \w+) bag").unwrap();
        let matches = re.captures(input).unwrap();
        let number: usize = matches[1].parse().unwrap();
        let color: String = matches[2].parse().unwrap();
        Self { color, number }
    }
}

fn possible_containers(regs: &[Regulation], containers_for: &str) -> Vec<String> {
    let mut containers: Vec<String> = Vec::new();
    let applicable_regs = regs.iter().filter(|r| r.contains.iter().any(|b| b.color == containers_for));
    for reg in applicable_regs {
        containers.push(reg.color.clone()); // direct containers
        containers.append(&mut possible_containers(&regs, &reg.color)); // recursive containers
    }
    containers
}

fn bag_count(regs: &[Regulation], containers_for: &str) -> usize {
    regs
        .iter()
        .filter(|r| r.color == containers_for)
        .map(|r| {
            r.contains.iter().map(|x| x.number).sum::<usize>() + // direct containers
            r.contains.iter().map(|c| &c.number*bag_count(&regs, &c.color)).sum::<usize>() // recursive containers
        })
        .sum()
}

fn day07(input: &str) {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    let regs: Vec<Regulation> = input.iter().map(Regulation::from).collect();

    // Part 1
    let part1 = possible_containers(&regs, "shiny gold").iter().unique().count();
    println!("Part 1: {}", part1); // 348

    // Part 2
    let part2 = bag_count(&regs, "shiny gold");
    println!("Part 2: {}", part2); // 18885

}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day07(&filename);
}