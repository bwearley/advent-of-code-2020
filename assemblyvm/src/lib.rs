pub mod assemblyvm {

    #[derive(Clone)]
    pub struct Instruction {
        pub instr: String,
        pub value: i64,
        pub runs: u64,
    }
    impl From<&String> for Instruction {
        fn from(input: &String) -> Self {
            let mut parts = input.split_ascii_whitespace();
            Self {
                instr: parts.next().unwrap().parse().unwrap(),
                value: parts.next().unwrap().parse().unwrap(),
                runs: 0,
            }
        }
    }

    #[derive(Clone)]
    pub struct AssemblyVM {
        pub ptr: i64,
        pub accum: i64,
        pub code: Vec<Instruction>,
        pub code_default: Vec<Instruction>,
        pub loop_detected: bool,
        pub active: bool,
        // Options
        pub break_on_loop: bool,
        pub debug: bool,
    }
    impl AssemblyVM {
        pub fn new(code: Vec<String>) -> Self {
            let code: Vec<_> = code.iter().map(Instruction::from).collect();
            Self {
                ptr: 0,
                accum: 0,
                code: code.clone(),
                code_default: code.clone(),
                loop_detected: false,
                active: true,
                // Options
                break_on_loop: false,
                debug: false,
            }
        }

        pub fn reset(&mut self) {
            self.ptr = 0;
            self.accum = 0;
            self.code = self.code_default.clone();
            self.loop_detected = false;
            self.active = false;
        }

        pub fn run(&mut self) {
            self.active = true;
            loop {
                let ptr = self.ptr as usize;
                if ptr >= self.code.len() { break; }
                let instr = &self.code[ptr];
                if self.break_on_loop && self.code[ptr].runs == 1 {
                    self.loop_detected = true;
                    self.active = false;
                    break;
                }
                
                // Process instruction
                match instr.instr.as_ref() {
                    "acc" => {
                        self.accum += instr.value;
                        self.ptr += 1;
                    },
                    "jmp" => self.ptr += instr.value,
                    "nop" => self.ptr += 1,
                    other => panic!("ERROR: Unknown instruction: {}", other),
                }
                self.code[ptr].runs += 1;
            } 
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn program_from_string(input: &str) -> Vec<String> {
        input.split("\n").map(String::from).collect()
    }

    #[test]
    fn day08_test_1() {
        let sample = program_from_string("nop +0
        acc +1
        jmp +4
        acc +3
        jmp -3
        acc -99
        acc +1
        jmp -4
        acc +6");
        let mut vm = assemblyvm::AssemblyVM::new(sample);
        vm.break_on_loop = true;
        vm.run();
        assert!(vm.accum == 5)
    }

    #[test]
    fn day08_test_2() {
        let sample = program_from_string("nop +0
        acc +1
        jmp +4
        acc +3
        jmp -3
        acc -99
        acc +1
        nop -4
        acc +6");
        let mut vm = assemblyvm::AssemblyVM::new(sample);
        vm.break_on_loop = true; // unnecessary but keeping for testing
        vm.run();
        assert!(vm.accum == 8)
    }

}