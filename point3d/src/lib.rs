#[macro_use]
extern crate text_io;

pub mod point3d {
    use std::ops::{Add,AddAssign};

    #[derive(Debug, Copy, Clone, Hash)]
    pub struct Point3D {
        pub x: i64,
        pub y: i64,
        pub z: i64,
    }

    impl Point3D {
        pub fn new(x: i64, y: i64, z: i64) -> Self {
            Self {
                x: x, y: y, z: z,
            }
        }
        pub fn zeros() -> Self {
            Self {
                x: 0, y: 0, z: 0,
            }
        }
        pub fn as_tuple(self) -> (i64,i64,i64) {
            (self.x, self.y, self.z)
        }
    }
    impl Add for Point3D {
        type Output = Self;
        fn add(self, other: Self) -> Self {
            Self {
                x: self.x + other.x,
                y: self.y + other.y,
                z: self.z + other.z,
            }
        }
    }
    impl AddAssign for Point3D {
        fn add_assign(&mut self, other: Self) {
            *self = Self {
                x: self.x + other.x,
                y: self.y + other.y,
                z: self.z + other.z,
            };
        }
    }
    impl PartialEq for Point3D {
        fn eq(&self, other: &Self) -> bool {
            self.x == other.x && self.y == other.y && self.z == other.z
        }
    }
    impl Eq for Point3D {}

}
