use std::env;
use std::io::{self};

const NUM_CUPS_P1: usize = 9;
const NUM_CUPS_P2: usize = 1_000_000;

#[derive(Debug,Clone)]
struct Cup {
    cw: usize,
    ccw: usize,
}

fn part1(input: &str) -> io::Result<()> {
    let cups_order = std::fs::read_to_string(input)
        .expect("Input file not found.")
        .chars()
        .map(|c| c.to_digit(10).unwrap() as usize)
        .collect::<Vec<_>>();

    let mut cups = vec![Cup {cw: 0, ccw: 0}; NUM_CUPS_P1+1];
    
    // Initialize cups
    // cups[0] is dead
    for ix in 0..NUM_CUPS_P1 {
        let label = cups_order[ix];
        let ixp1 = if ix+1 == NUM_CUPS_P1 { 0 } else { ix + 1 };
        let ixm1 = if ix   == 0 { NUM_CUPS_P1-1 } else { ix - 1};
        cups[label].cw  = cups_order[ixp1];
        cups[label].ccw = cups_order[ixm1];
    }

    let mut current = cups_order[0];
    let mut destination;
    for _ in 0..100 {
        let selected = [
            cups[current].cw,
            cups[cups[current].cw].cw,
            cups[cups[cups[current].cw].cw].cw];

        destination = current - 1;
        if destination == 0 { destination = NUM_CUPS_P1; }
        'dest_lp: loop {
            if destination != selected[0] &&
               destination != selected[1] &&
               destination != selected[2] { break 'dest_lp;
            } else {
                destination -= 1;
                if destination == 0 { destination = NUM_CUPS_P1; }
            }
        }

        // Split 3 cups out of chain
        let chain_cw_save = cups[selected[2]].cw; // clockwise end of 3 cup chain
        cups[chain_cw_save].ccw = current;
        cups[current].cw = chain_cw_save;

        // Insert 3 cups
        let dest_cw_save = cups[destination].cw;
        cups[destination].cw = selected[0];
        cups[selected[0]].ccw = destination;
        cups[selected[2]].cw = dest_cw_save;
        cups[dest_cw_save].ccw = selected[2];

        current = cups[current].cw;
    }

    // Build answer string
    let mut ans: Vec<usize> = Vec::new();
    let mut next = cups[1].cw;
    for _ in 0..NUM_CUPS_P1-1 {
        ans.push(next);
        next = cups[next].cw;
    }
    let ans = ans.iter().map(|x| format!("{}",x)).collect::<String>();

    println!("Part 1: {}", ans); // 26354798
    
    Ok(())
}

fn part2(input: &str) -> io::Result<()> {
    let cups_order = std::fs::read_to_string(input)
        .expect("Input file not found.")
        .chars()
        .map(|c| c.to_digit(10).unwrap() as usize)
        .collect::<Vec<_>>();

    let mut cups = vec![Cup {cw: 0, ccw: 0}; NUM_CUPS_P2+1];
    
    // Initialize cups
    // cups[0] is dead
    for ix in 0..NUM_CUPS_P1 {
        let label = cups_order[ix];
        let ixp1 = if ix+1 == NUM_CUPS_P1 { NUM_CUPS_P1+1 } else { cups_order[ix+1] };
        let ixm1 = if ix   == 0 { NUM_CUPS_P2 } else { cups_order[ix-1] };
        cups[label].cw  = ixp1;
        cups[label].ccw = ixm1;
    }
    for ix in NUM_CUPS_P1+1..=NUM_CUPS_P2 {
        let label = ix;
        let ixp1 = if ix+1 == NUM_CUPS_P2+1 { cups_order[0] } else { ix + 1 };
        let ixm1 = if ix-1 == NUM_CUPS_P1 { cups_order[NUM_CUPS_P1-1] } else { ix - 1 };
        cups[label].cw  = ixp1;
        cups[label].ccw = ixm1;
    }

    let mut current = cups_order[0];
    let mut destination;
    for _ in 0..10_000_000 {
        let selected = [
            cups[current].cw,
            cups[cups[current].cw].cw,
            cups[cups[cups[current].cw].cw].cw];

        destination = current - 1;
        if destination == 0 { destination = NUM_CUPS_P2; }
        'dest_lp: loop {
            if destination != selected[0] &&
               destination != selected[1] &&
               destination != selected[2] { break 'dest_lp;
            } else {
                destination -= 1;
                if destination == 0 { destination = NUM_CUPS_P2; }
            }
        }

        // Split 3 cups out of chain
        let chain_cw_save = cups[selected[2]].cw; // clockwise end of 3 cup chain
        cups[chain_cw_save].ccw = current;
        cups[current].cw = chain_cw_save;

        // Insert 3 cups
        let dest_cw_save = cups[destination].cw;
        cups[destination].cw = selected[0];
        cups[selected[0]].ccw = destination;
        cups[selected[2]].cw = dest_cw_save;
        cups[dest_cw_save].ccw = selected[2];

        current = cups[current].cw;
    }

    let cup1 = cups[1].cw;
    let cup2 = cups[cups[1].cw].cw;
    let part2 = cup1 * cup2;

    println!("Part 2: {}", part2); // 166298218695
    
    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    part1(&filename).unwrap();
    part2(&filename).unwrap();
}