use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

#[derive(Debug,Clone)]
struct Bus {
    id: i64,
    offset: i64,
}

fn day13(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    // Part 1
    let earliest = input.get(0).unwrap().parse::<i64>().unwrap();
    let mut busses = input
        .get(1)
        .unwrap()
        .split(",")
        .filter(|&x| x != "x")
        .map(|x| x.parse::<i64>().unwrap())
        .collect::<Vec<_>>();
    busses.sort();

    'wait_lp: for wait in 0.. {
        for bus in &busses {
            if (wait + earliest) % bus == 0 {
                println!("Part 1: {}", wait * bus); // 138
                break 'wait_lp;
            }
        }
    }

    // Part 2
    let busses = input
        .get(1)
        .unwrap()
        .split(",")
        .enumerate()
        .filter(|&(_,b)| b != "x")
        .map(|(i,b)| Bus { id: b.parse().unwrap(), offset: i as i64 })
        .collect::<Vec<_>>();
    
    let mut t = 0;
    'main_lp: loop {
        let mut tskip = 1;
        'inner: for bus in &busses {
            if (t + bus.offset) % bus.id != 0 { break 'inner; }
            tskip *= bus.id; // accumulates the product of the first N bus IDs that match for this t
        }
        if busses.iter().map(|b| (t + b.offset) % b.id).sum::<i64>() == 0 { break 'main_lp; }
        t += tskip;
    }
    println!("Part 2: {}", t); // 226845233210288

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day13(&filename).unwrap();
}