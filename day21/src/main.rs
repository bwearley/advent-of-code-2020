use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::HashMap;

fn day21(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    let mut allergen_possibilities: HashMap<String,Vec<String>> = HashMap::new(); 

    // Build map of allergens and possible ingredients for each
    for line in &input {
        let line = line.replace("(","").replace(")","").replace(",","");
        let parts = line.split("contains").collect::<Vec<_>>();
        let ingredients = parts[0].split_ascii_whitespace().collect::<Vec<_>>();
        let allergens   = parts[1].split_ascii_whitespace().collect::<Vec<_>>();
        for allergen in &allergens {
            let current = allergen_possibilities.entry(allergen.to_string()).or_insert(Vec::new());
            for ingr in &ingredients {
                current.push(ingr.to_string());
            }
        }
    }

    // Deduplicate ingredients
    let keys = allergen_possibilities.clone();
    let keys = keys.keys();
    for key in keys {
        if let Some(x) = allergen_possibilities.get_mut(key) {
            x.sort();
            x.dedup();
        }
    }

    // Reprocess input to eliminate possibilities from map
    for line in &input {
        let line = line.replace("(","").replace(")","").replace(",","");
        let parts = line.split("contains").collect::<Vec<_>>();
        let ingredients = parts[0].split_ascii_whitespace().collect::<Vec<_>>();
        let allergens = parts[1].split_ascii_whitespace().collect::<Vec<_>>();

        for allergen in &allergens {
            if let Some(current) = allergen_possibilities.get_mut(&allergen.to_string()) {
                // Select ingredients to remove
                let mut remove: Vec<String> = Vec::new();
                for ingr in current.iter() {
                    if &ingredients.iter().filter(|i| i.to_string() == *ingr).count() < &1 {
                        remove.push(ingr.to_string());
                    }
                }
                for rem in remove {
                    let ix = current.iter().position(|x| *x == rem).unwrap();
                    current.remove(ix);
                }
            }
        }
    }

    // List of allergens and possible ingredients
    for (allergen,ingrds) in &allergen_possibilities {
        println!("{}: {:?}", allergen, ingrds);
    }

    // Get unique ingredients from allergen map
    let mut possible_allergens: Vec<String> = Vec::new();
    for (_,ingrds) in allergen_possibilities.iter() {
        for ingr in ingrds {
            possible_allergens.push(ingr.to_string());
        }
    }
    possible_allergens.sort();
    possible_allergens.dedup();

    println!("Allergens: {:?}", possible_allergens);

    // Count instances of non-allergen ingredients
    let mut part1 = 0;
    for line in &input {
        let line = line.replace("(","").replace(")","").replace(",","");
        let parts = line.split("contains").collect::<Vec<_>>();
        let ingredients = parts[0].split_ascii_whitespace().collect::<Vec<_>>();
        for ingr in ingredients {
            if possible_allergens.iter().position(|x| x == ingr).is_none() {
                part1 += 1;
            }
        }
    }
    println!("Part 1: {}", part1); // 2203

    // Part 2
    let num_allergens = possible_allergens.len();
    let allergies = allergen_possibilities.clone();
    let allergies = allergies.keys().collect::<Vec<_>>();
    let mut actuals: HashMap<String,String> = HashMap::new();
    while actuals.len() != num_allergens {
        'allergen_lp: for allergen in allergies.iter() {
            let mut j: String = String::new();
            if let Some(x) = allergen_possibilities.get(allergen.clone()) {
                if x.len() != 1 { continue 'allergen_lp; }
                j = x.iter().next().unwrap().to_string();
                actuals.insert(allergen.to_string(),j.clone());
            }
            for other_allergen in allergies.iter() {
                if other_allergen == allergen { continue; }
                if let Some(x) = allergen_possibilities.get_mut(other_allergen.clone()) {
                    if let Some(ix) = x.iter().position(|a| a == &j) {
                        x.remove(ix);
                    }

                }
            }
        }
    }

    println!("Finals:");
    for (k,v) in &actuals {
        println!("{}: {}", k,v);
    }
    println!();

    let sorted = actuals.clone();
    let mut sorted = sorted.keys().collect::<Vec<_>>();
    sorted.sort();

    let mut part2 = String::new();
    for (i,item) in sorted.iter().enumerate() {
        part2.push_str(&actuals.get(item.clone()).unwrap());
        if i != sorted.len() - 1 { part2.push_str(",");}
    }
    println!("Part 2: {}", part2); // fqfm,kxjttzg,ldm,mnzbc,zjmdst,ndvrq,fkjmz,kjkrm

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day21(&filename).unwrap();
}